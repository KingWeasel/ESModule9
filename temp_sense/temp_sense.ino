/*
   pwm_dac.ino

   Jacob Kunnappally
   October 2017

   What this program does:

   Setup
   -defines, globals, serial init. the usual
   -configure ADC1 to use internal temp sensor
   -memset our circular buffer

   Runtime
   -Continuously overwrite a circular buffer element every
    10 seconds
   -Report when temp drops below freezing in internal temp
    sensor
*/

#define BOARD_LED_PIN 13
#define V_25 1.42 // V
#define AVG_SLOPE 0.0043 // V/degC

double circ_buf[10];

void setup()
{
  setup_temperature_sensor();
  memset(circ_buf, 0, sizeof(circ_buf));
  pinMode(BOARD_LED_PIN, OUTPUT);
}

void loop()
{
  double adc_V, tempC;
  bool freeze;

  for (int i = 0; i < 10; i++)
  {
    adc_V = adc_read(ADC1, 16) / 1241.212121; //V
    tempC = ((V_25 - adc_V) / AVG_SLOPE) + 25.0;
    //don't want to make assumptions about going from Volts
    //straight to Fahrenheit, so just converting the Celsius
    //conversion from the reference manual
    circ_buf[i] = (1.8 * tempC) + 32;

    Serial.print("Current Temp: ");
    Serial.print(circ_buf[i]);
    Serial.println(" deg F");

    if (freeze = circ_buf[i] <= 32.00)
    {
      Serial.println("It's freezing!");
    }

    digitalWrite(BOARD_LED_PIN, freeze);

    delay(10000);
  }
}

void setup_temperature_sensor()
{ //got this from http://forums.leaflabs.com/forums.leaflabs.com/topic21e2.html?id=1120
  adc_reg_map *regs = ADC1->regs;

  // Set the TSVREFE bit in the ADC control register 2 
  // (ADC_CR2) to wake up the
  // temperature sensor from power down mode. 

  regs->CR2 |= ADC_CR2_TSVREFE;//had to change typo here. Was "TSEREFE"

  // Select a sample time of 17.1 μs
  // set channel 16 sample time to 239.5 cycles
  // 239.5 cycles of the ADC clock (72MHz/6=12MHz) 
  //is over 17.1us (about 20us), but no smaller
  // sample time exceeds 17.1us.

  regs->SMPR1 = (0b111 << (3 * 6));   // set channel 16, the temp. sensor
}





